<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Hello ${name}!</title>
<link href="/css/main.css" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
	<h1 class="display-4">Compte � decouvert :</h1>
	<br>
<table class="table table-sm">
  <thead>
    <tr>
      <th scope="col">Nom</th>
      <th scope="col">Solde</th>
      <th scope="col">D�couvert autoris�</th>
    </tr>
  </thead>
  <tbody>
  <c:forEach items="${compteDecouvert}" var="compte">
    <tr>
      <th scope="row">${compte.nom}</th>
      <td>${compte.solde}</td>
      <td>${compte.decouvertAutorise}</td>
    </tr>
    </c:forEach>
  </tbody>
</table>
	<br>
	<h1 class="display-4">Message re�u :</h1>
	<table class="table table-sm">
  <thead>
    <tr>
      <th scope="col">Nom du client</th>
      <th scope="col">Objet</th>
      <th scope="col">Contenu</th>
    </tr>
  </thead>
  <tbody>
  <c:forEach items="${messages}" var="message">
    <tr>
      <th scope="row">${message.client.nom}</th>
      <td>${message.objet}</td>
      <td>${message.contenu}</td>
    </tr>
    </c:forEach>
  </tbody>
  </table>
  
  <h1 class="display-4">Tous les comptes :</h1>
	<table class="table table-sm">
  <thead>
    <tr>
      <th scope="col">Nom</th>
      <th scope="col">Solde</th>
      <th scope="col">D�couvert autoris�</th>
      <td></td>
    </tr>
  </thead>
  <tbody>
  <c:forEach items="${comptes}" var="compte">
    <tr>
      <th scope="row">${compte.nom}</th>
      <td>${compte.solde}</td>
      <td>${compte.decouvertAutorise}</td>
      <td><a href="${pageContext.request.contextPath}/conseiller/compte/<c:out value="${compte.idCompte}"/>"> Voir</a></td>
    </tr>
    </c:forEach>
  </tbody>
</table>
	
	
	<h1 class="display-4">Ajouter un compte :</h1>
	<div class="card text-white bg-primary" style="width: 25rem;">
  <div class="card-body">
    <p class="card-text">
    <form method="post" action="${pageContext.request.contextPath}/conseiller/addCompte">
  	<input type="hidden" name="idConseiller" value="<c:out value="${conseiller.idConseiller}"/>" />
  	<br>
	Nom : <input name="nom" />
	</br></br>
	Solde : <input name="solde"/>
	</br></br>
	D�couvert autoris� : <input name="decouvertAutorise" />
	</br></br>
	RIB : <input name="rib" />
	</br></br>
	<button class="btn btn-light" type="submit" value="Ajouter">Ajouter</button>
	</form>
	</p>
  </div>
</div>
	</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>	
</body>
</html>