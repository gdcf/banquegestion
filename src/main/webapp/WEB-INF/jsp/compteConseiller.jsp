<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title></title>
<link href="/css/main.css" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<h1 class="display-4">Edition du compte :</h1>
	<div class="card text-white bg-primary" style="width: 25rem;">
  <div class="card-body">
    <p class="card-text">
    <form method="post" action="${pageContext.request.contextPath}/conseiller/updateCompte">
  	<input type="hidden" name="idCompte" value="<c:out value="${compte.idCompte}"/>" />
	<br>
	Nom : <input name="nom" value="<c:out value="${compte.nom}"/>" />
	<br><br>
	Solde : <input name="solde" value="<c:out value="${compte.solde}"/>" />
	<br><br>
	D�couvert autoris� : <input name="decouvertAutorise" value="<c:out value="${compte.decouvertAutorise}"/>" />
	<br><br>
	RIB : <input name="rib" value="<c:out value="${compte.rib}"/>" />
	<br><br>
	<button class="btn btn-light" type="submit" value="Modifier">Modifier</button>
	</form>
	</p>
  </div>
</div>
<h1 class="display-4">Les transactions du compte :</h1>
	</br>
	<ul class="list-group">
	<c:forEach items="${transactions}" var="transaction">
	  <li class="list-group-item">Nom : ${transaction.nom}, Date: ${transaction.date}, Montant: ${transaction.montant} euros</li>
	  </c:forEach>
	</ul>
	
	<br>
	<h1 class="display-4">Versement :</h1>
	<form method="post" action="${pageContext.request.contextPath}/conseiller/versementCompte">
  	<input type="hidden" name="idCompte" value="<c:out value="${compte.idCompte}"/>" />
	<br>
	Montant : <input name="montant" />
	<input type="submit" value="Verser" />
	</form>
	<br>
	<h1 class="display-4">Virement :</h1>
	<form method="post" action="${pageContext.request.contextPath}/conseiller/virementCompte">
  	<input type="hidden" name="idCompte" value="<c:out value="${compte.idCompte}"/>" />
	Montant : <input name="montant" />
	<br><br>
		Compte de destination : <select name='compteReceveur'>
	    <c:forEach items="${comptes}" var="compte">
	            <option value="${compte.idCompte}">${compte.nom}</option>
	    </c:forEach>
		</select>
	<br>
	<input type="submit" value="Verser" />
	</form>
	</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>	
	
</body>
</html>