<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title></title>
<link href="/css/main.css" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<div class="container">
	</br>
	<h1 class="display-4">Mes comptes :</h1>
	</br>
	</table>
	<c:forEach items="${comptes}" var="compte">
		<div class="card text-white bg-primary w-100" style="max-width: 18rem;">
		  <div class="card-header">${compte.nom}</div>
		  <div class="card-body">
		    <p class="card-text">
			    <b>Solde : </b>${compte.solde}&#x20AC;</br>
			    <b>D&#233couvert autoris&#233 : </b>${compte.decouvertAutorise}&#x20AC;</br>
			    <a class="btn btn-dark" role="button" href="${pageContext.request.contextPath}/client/compte/<c:out value="${compte.idCompte}"/>">Voir</a>
		    </p>
		  </div>
		</div>
		<br>
	</c:forEach>
	<br>
	<h1 class="display-4">Mon agence :</h1>
	<ul class="list-group">
	  <li class="list-group-item">Nom : ${agence.nom}</li>
	  <li class="list-group-item">Num&#233ro de t&#233l&#233phone : ${agence.numeroTelephone}</li>
	  <li class="list-group-item">Adresse : ${agence.adresse}</li>
	  <li class="list-group-item">Horaires : ${agence.horaire}</li>
	</ul>
	<br>
	<h1 class="display-4">Envoyer un message &#224; son conseiller :</h1>
	<br>
	<div class="card">
  <div class="card-body">
    <p class="card-text">
	    <form method="post" action="${pageContext.request.contextPath}/client/sendMessage">
	  	<input type="hidden" name="idClient" value="<c:out value="${client.idClient}"/>" />
	  	<br>
		<p style="width:'20px;'">Objet :</p> <input name="objet" />
		<br>
		<p style="width:'20px;'">Contenu :</p> <input name="contenu" />
		<br><br>
	 <button class="btn btn-primary" type="submit">Envoyer</button>
		</form>
	</p>
  </div>
</div>
	
	<br><br>
	</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>