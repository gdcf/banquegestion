<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title></title>
<link href="/css/main.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<style>
.icons img{
    margin:1%;
}

.selecttab {
    border: 1px solid green;
    width: 100%;
    margin-bottom: 3%;
    
}

.tab {
    margin-top: 2%;
    margin-left: 10%;
    margin-right: 10%;
}

.tabresult {
    background-color: #eee;
}

.caption {
    background-color: #fff;
}

.textbox {
    text-decoration: underline;
}


#newform {
    display: none;
}

#existingform {
    display: none;
}

.tabselection {
    padding-top: 3%;
    font-size: 30px;
}

.viewtext {
    text-align: center;
    margin-top: -2%;
    padding: 0;
    border: 0;
    width: 100%;
    background: #fff;
}

#tab1selectionlist {
    display: none;
}

#tab2selectionlist {
    display: none;
}

#tab3selectionlist {
    display: none;
}
</style>
<body>
<div class="container">
	<div class="row">
	    <div class="col-lg-12">
	    </br></br></br></br></br></br></br></br></br></br></br></br></br></br></br>
	        <div class="row text-center">
    	        <h1>Choix du compte</h1>	            
	        </div>
	        <div class="row text-center icons">
	            <img src="">
	            <img src="">
	            <img src="">
	        </div>
	        <div class="row text-center tab">
	            <div class="col-lg-5">
	                <a class="btn btn-default selecttab" href="${pageContext.request.contextPath}/authentification/client" role="button">Je suis un client</a>
	            </div>
	           
	            <div class="col-lg-2">
	                <p>ou</p>
	            </div>
	            <div class="col-lg-5">
	                <a class="btn btn-default selecttab" href="${pageContext.request.contextPath}/authentification/conseiller" role="button">Je suis un conseiller</a>
	            </div>
	            
	       </div>
	       <div class="row tabresult">

         </div> 
         
        
        
	   
	           
	       </div>
	    </div>
	</div>
</div>
</body>
</html>