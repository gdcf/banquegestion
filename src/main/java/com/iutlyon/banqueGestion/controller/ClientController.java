package com.iutlyon.banqueGestion.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.iutlyon.banqueGestion.model.Client;
import com.iutlyon.banqueGestion.model.Compte;
import com.iutlyon.banqueGestion.model.CompteClient;
import com.iutlyon.banqueGestion.model.Message;
import com.iutlyon.banqueGestion.repository.ClientRepository;
import com.iutlyon.banqueGestion.repository.CompteClientRepository;
import com.iutlyon.banqueGestion.repository.CompteRepository;
import com.iutlyon.banqueGestion.repository.MessageRepository;
import com.iutlyon.banqueGestion.utils.ConnexionUtil;

@Controller
public class ClientController {

	@Autowired
	ClientRepository clientRepository;

	@Autowired
	CompteClientRepository compteClientRepository;

	@Autowired
	MessageRepository messageRepository;

	@Autowired
	ConnexionUtil connexionUtil;

	@RequestMapping(value= {"/client/{id}"}, method=RequestMethod.GET)
	public String client(Model model, @PathVariable("id") Long idClient,HttpServletRequest req, HttpServletResponse
			res) {

		HttpSession session = req.getSession();
		if (session.getAttribute("idcompte") != null && session.getAttribute("typecompte") != null)
		if (connexionUtil.droitClient(req, idClient))
		{
			Client client = clientRepository.getOne(idClient);
			List<Compte> comptes = new ArrayList<Compte>();
			for(CompteClient compteClient : compteClientRepository.findByClientIdClient(idClient))
				comptes.add(compteClient.getCompte());
			model.addAttribute("comptes", comptes);
			model.addAttribute("agence", client.getConseiller().getAgence());
			model.addAttribute("client",client);
			return "client";
		}
		return "pageerreur";
	}

	@RequestMapping(value= {"/client/sendMessage"})
	public String sendMessage(Model model, String idClient, String objet, String contenu, HttpServletRequest req) {
		if (connexionUtil.droitClient(req, Long.parseLong(idClient)))
				{
			Client client = clientRepository.getOne(Long.parseLong(idClient));
			Message message = new Message();
			message.setClient(client);
			message.setConseiller(client.getConseiller());
			message.setContenu(contenu);
			message.setObjet(objet);
			messageRepository.save(message);
			return "redirect:/client/" + idClient;
				}
		return "pageerreur";
	}



}
