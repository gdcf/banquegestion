package com.iutlyon.banqueGestion.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.iutlyon.banqueGestion.model.Client;
import com.iutlyon.banqueGestion.model.Compte;
import com.iutlyon.banqueGestion.model.CompteClient;
import com.iutlyon.banqueGestion.model.Conseiller;
import com.iutlyon.banqueGestion.model.Message;
import com.iutlyon.banqueGestion.repository.ClientRepository;
import com.iutlyon.banqueGestion.repository.CompteClientRepository;
import com.iutlyon.banqueGestion.repository.ConseillerRepository;
import com.iutlyon.banqueGestion.repository.MessageRepository;
import com.iutlyon.banqueGestion.utils.ConnexionUtil;

@Controller
public class ConseillerController {

	@Autowired
	ConseillerRepository conseillerRepositoy;

	@Autowired
	ClientRepository clientRepository;

	@Autowired
	CompteClientRepository compteClientRepository;

	@Autowired
	MessageRepository messageRepository;

	@Autowired
	ConnexionUtil connexionUtil;

	@RequestMapping(value= {"/conseiller/{id}"}, method=RequestMethod.GET)
	public String accueilConseiller(Model model, @PathVariable("id") Long idConseiller,HttpServletRequest req, HttpServletResponse
			res) {
		if (connexionUtil.droitConseiller(req, idConseiller))
		{
			Conseiller conseiller = conseillerRepositoy.findById(idConseiller).get();
			List<Compte> compteDecouvert = new ArrayList<Compte>();
			List<Compte> comptes = new ArrayList<Compte>();
			List<Message> messages = new ArrayList<Message>();
			List<Client> mesClients = clientRepository.findByConseillerIdConseiller(idConseiller);
			for(Client client : mesClients)
			{
				for(Message message : messageRepository.findByClientIdClientAndConseillerIdConseiller(client.getIdClient(), idConseiller))
				{
					messages.add(message);
				}


				for(CompteClient compteClient : compteClientRepository.findByClientIdClient(client.getIdClient()))
				{
					Compte compte = compteClient.getCompte();
					comptes.add(compte);
					if(compte.getSolde() < compte.getDecouvertAutorise())
					{
						compteDecouvert.add(compte);
					}
				}
			}

			model.addAttribute("compteDecouvert", compteDecouvert);
			model.addAttribute("comptes", comptes);
			model.addAttribute("messages", messages);
			model.addAttribute("conseiller",conseiller);
			return "conseiller";

		}
		return "pageerreur";

	}
}
