package com.iutlyon.banqueGestion.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.ClientEndpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.iutlyon.banqueGestion.metier.CompteMetier;
import com.iutlyon.banqueGestion.model.Compte;
import com.iutlyon.banqueGestion.model.CompteClient;
import com.iutlyon.banqueGestion.model.Transaction;
import com.iutlyon.banqueGestion.repository.ClientRepository;
import com.iutlyon.banqueGestion.repository.CompteClientRepository;
import com.iutlyon.banqueGestion.repository.CompteRepository;
import com.iutlyon.banqueGestion.repository.ConseillerRepository;
import com.iutlyon.banqueGestion.repository.TransactionRepository;
import com.iutlyon.banqueGestion.utils.ConnexionUtil;

@Controller
public class CompteController {

	@Autowired
	CompteRepository compteRepository;

	@Autowired
	TransactionRepository transactionRepository;

	@Autowired
	ClientRepository clientRepository;

	@Autowired
	CompteClientRepository compteClientRepository;

	@Autowired
	ConseillerRepository conseillerRepository;

	@Autowired
	CompteMetier compteMetier;

	@Autowired
	ConnexionUtil connexionUtil;

	//////////////////////////PARTIE GESTION COMPTES CONSEILLER
	@RequestMapping(value= {"/conseiller/compte/{id}"}, method=RequestMethod.GET)
	public String conseillerCompte(Model model, @PathVariable("id") Long idCompte,HttpServletRequest req) {

		Compte compte = compteRepository.getOne(idCompte);
		if(connexionUtil.droitConseiller(req, compte.getConseiller().getIdConseiller()))
		{

			model.addAttribute("compte", compte);
			model.addAttribute("transactions", transactionRepository.findByCompteIdCompte(idCompte));
			model.addAttribute("comptes", compteRepository.findAll());
			return "compteConseiller";
		}
		return "pageerreur";
	}

	@RequestMapping(value= {"/conseiller/updateCompte"})
	public String updateCompte(Model model,String idCompte, String nom, String solde, String decouvertAutorise, 
			String rib ,HttpServletRequest req) {

		Compte compte = compteRepository.getOne(Long.parseLong(idCompte));
		if(connexionUtil.droitConseiller(req, compte.getConseiller().getIdConseiller()))
		{

			compte.setNom(nom);
			compte.setSolde(Short.parseShort(solde));
			compte.setDecouvertAutorise(Short.parseShort(decouvertAutorise));
			compte.setRib(rib);
			compteRepository.save(compte);
			return "redirect:/conseiller/compte/" + compte.getIdCompte();
		}
		return "pageerreur";
	}

	@RequestMapping(value= {"/conseiller/suppressionCompte/{id}"})
	public String supressionCompte(Model model, @PathVariable("id") Long idCompte, HttpServletRequest req) {

		Compte compte = compteRepository.getOne(idCompte);
		if(connexionUtil.droitConseiller(req, compte.getConseiller().getIdConseiller()))
		{
			compteRepository.delete(compte);
			return "redirect:/conseiller/" + compte.getConseiller().getIdConseiller() ;
		}
		return "pageerreur";
	}

	@RequestMapping(value= {"/conseiller/addCompte"})
	public String supressionCompte(Model model, String idConseiller, String nom, String solde, String decouvertAutorise,
			String rib, HttpServletRequest req) {
		if(connexionUtil.droitConseiller(req, Long.parseLong(idConseiller)))
		{
			Compte compte = new Compte();
			compte.setNom(nom);
			compte.setSolde(Short.parseShort(solde));
			compte.setDecouvertAutorise(Short.parseShort(decouvertAutorise));
			compte.setConseiller(conseillerRepository.getOne(Long.parseLong(idConseiller)));
			compte.setRib(rib);
			compteRepository.save(compte);
			return "redirect:/conseiller/compte/" + compte.getIdCompte();
		}
		return "pageerreur";
	}

	@RequestMapping(value= {"/conseiller/versementCompte"})
	public String versementCompte(Model model, String idCompte, String montant, HttpServletRequest req) {

		Compte compte = compteRepository.getOne(Long.parseLong(idCompte));
		if(connexionUtil.droitConseiller(req, compte.getConseiller().getIdConseiller()))
		{
			compte.setSolde((short)(compte.getSolde() + Short.parseShort(montant)));
			compteRepository.save(compte);

			Transaction transaction = new Transaction();
			transaction.setCompte(compte);
			transaction.setDate(new Date());
			transaction.setMontant(Short.parseShort(montant));
			transaction.setNom("VERSEMENT");
			transactionRepository.save(transaction);

			return "redirect:/conseiller/compte/" + compte.getIdCompte();
		}
		return "pageerreur";
	}

	@RequestMapping(value= {"/conseiller/virementCompte"})
	public String virementCompte(Model model, String idCompte, String montant, String compteReceveur,HttpServletRequest req) {

		if(connexionUtil.droitConseiller(req, compteRepository.getOne(Long.parseLong(idCompte)).getConseiller().getIdConseiller()))
		{
			compteMetier.execVirement(idCompte, montant, compteReceveur);

			return "redirect:/conseiller/compte/" + idCompte;
		}
		return "pageerreur";
	}

	//PARTIE GESTION COMPTES CLIENT
	@RequestMapping(value= {"/client/compte/{id}"}, method=RequestMethod.GET)
	public String clientCompte(Model model, @PathVariable("id") Long idCompte, HttpServletRequest req) {
		Boolean droit = false;
		for(CompteClient compteClient : compteClientRepository.findByCompteIdCompte(idCompte))
		{
			if(connexionUtil.droitClient(req, compteClient.getIdClient()))
				droit=true;
		}
		if(droit)
		{
			Compte compte = compteRepository.getOne(idCompte);
			model.addAttribute("compte", compte);
			model.addAttribute("transactions", transactionRepository.findByCompteIdCompte(idCompte));
			model.addAttribute("comptes", compteRepository.findAll());
			return "compteClient";
		}
		return "pageerreur";
	}

	@RequestMapping(value= {"/client/virementCompte"})
	public String clientVirementCompte(Model model, String idCompte, String montant, String compteReceveur, HttpServletRequest req) {
		Boolean droit = false;
		for(CompteClient compteClient : compteClientRepository.findByCompteIdCompte(Long.parseLong(idCompte)))
		{
			if(connexionUtil.droitClient(req, compteClient.getIdClient()))
				droit=true;
		}
		if(droit)
		{
			compteMetier.execVirement(idCompte, montant, compteReceveur);

			return "redirect:/client/compte/" + idCompte;
		}
		return "pageerreur";
	}

	@RequestMapping(value= {"/client/updateRib"})
	public String updateRib(Model model, String idCompte, String rib, HttpServletRequest req){
		Boolean droit = false;
		for(CompteClient compteClient : compteClientRepository.findByCompteIdCompte(Long.parseLong(idCompte)))
		{
			if(connexionUtil.droitClient(req, compteClient.getIdClient()))
				droit=true;
		}
		if(droit)
		{
			Compte compte = compteRepository.getOne(Long.parseLong(idCompte));
			compte.setRib(rib);
			compteRepository.save(compte);

			return "redirect:/client/compte/" + idCompte;
		}
		return "pageerreur";
	}


}
