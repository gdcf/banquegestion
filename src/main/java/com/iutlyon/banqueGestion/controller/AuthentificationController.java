package com.iutlyon.banqueGestion.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.iutlyon.banqueGestion.model.*;
import com.iutlyon.banqueGestion.repository.ClientRepository;
import com.iutlyon.banqueGestion.repository.CompteClientRepository;
import com.iutlyon.banqueGestion.repository.CompteRepository;
import com.iutlyon.banqueGestion.repository.ConseillerRepository;
import com.iutlyon.banqueGestion.repository.MessageRepository;
import javax.servlet.*;

import javax.servlet.http.*;

import java.io.*;

@Controller
public class AuthentificationController {
	
	@Autowired
	ConseillerRepository conseillerRepository;
	
	@Autowired
	ClientRepository clientRepository;
	
	@RequestMapping(value= {"/"}, method=RequestMethod.GET)
	public String baseAuthentification(Model model,HttpServletRequest req, HttpServletResponse res) {
		HttpSession session = req.getSession();
		session.removeAttribute("idcompte");
		session.removeAttribute("typecompte");
		return "authent";
	}
	
	@RequestMapping(value= {"/authentification/client"})
	public String authentificationClient(Model model,String username, String password,HttpServletRequest req, HttpServletResponse
			res) {

		{
			HttpSession session = req.getSession();
			session.removeAttribute("idcompte");
			session.removeAttribute("typecompte");
			if (username != null || password != null)
			{
			List<Client> user = clientRepository.findByCodeClient(username);
			if (user.isEmpty())
			{
				model.addAttribute("erreur","Le nom de compte spécifié n'existe pas.");
				return "authentclient";
			}
			else
			{
				if (password == null || !user.get(0).getPassword().equals(password))
				{
					model.addAttribute("erreur","Le mot de passe est incorrect.");
					return "authentclient";
				}
				else
				{
					session.setAttribute("idcompte", user.get(0).getIdClient());
					session.setAttribute("typecompte","client");
					return "redirect:/client/" + user.get(0).getIdClient();
				}
			}
			}
		return "authentclient";
		
	}
	}
	
	@RequestMapping(value= {"/authentification/conseiller"})
	public String authentificationConseiller(Model model,String username, String password,HttpServletRequest req, HttpServletResponse
			res) {

		{
			HttpSession session = req.getSession();
			session.removeAttribute("idcompte");
			session.removeAttribute("typecompte");
			if (username != null || password != null)
			{
			List<Conseiller> user = conseillerRepository.findByCodeConseiller(username);
			if (user.isEmpty())
			{
				model.addAttribute("erreur","Le nom de compte spécifié n'existe pas.");
				return "authentconseiller";
			}
			else
			{
				if (password == null ||!user.get(0).getPassword().equals(password))
				{
					model.addAttribute("erreur","Le mot de passe est incorrect.");
					return "authentconseiller";
				}
				else
				{
					session.setAttribute("idcompte", user.get(0).getIdConseiller());
					session.setAttribute("typecompte","conseiller");
					return "redirect:/conseiller/" + user.get(0).getIdConseiller();
				}
			}
			}
		return "authentconseiller";
		
	}
	}
	
}
