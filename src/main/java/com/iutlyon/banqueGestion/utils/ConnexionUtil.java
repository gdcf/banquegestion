package com.iutlyon.banqueGestion.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

@Service
public class ConnexionUtil {



	public Boolean droitClient(HttpServletRequest req, Long idClient)
	{
		Boolean droit = false;
		HttpSession session = req.getSession();
		if (session.getAttribute("idcompte") != null)
		{
			if(session.getAttribute("idcompte").equals(idClient) && session.getAttribute("typecompte").equals("client"))
			{
				droit = true;
			}

		}
		return droit;
	}
	
	public Boolean droitConseiller(HttpServletRequest req, Long idConseiller)
	{
		Boolean droit = false;
		HttpSession session = req.getSession();
		if (session.getAttribute("idcompte") != null && session.getAttribute("typecompte") != null)
		{
			if(session.getAttribute("idcompte").equals(idConseiller) && session.getAttribute("typecompte").equals("conseiller"))
			{
				droit = true;
			}
		}
		return droit;
	}

}
