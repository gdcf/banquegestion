package com.iutlyon.banqueGestion.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iutlyon.banqueGestion.model.Agence;
import com.iutlyon.banqueGestion.model.CompteClient;
import com.iutlyon.banqueGestion.model.CompteClientPK;

@Transactional
public interface CompteClientRepository extends JpaRepository<CompteClient, CompteClientPK> {

	List<CompteClient> findByClientIdClient(Long idClient);
	List<CompteClient> findByCompteIdCompte(Long idCompte);
}
