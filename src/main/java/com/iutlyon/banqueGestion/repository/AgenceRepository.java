package com.iutlyon.banqueGestion.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iutlyon.banqueGestion.model.Agence;

@Transactional
public interface AgenceRepository extends JpaRepository<Agence, Long> {

}
