package com.iutlyon.banqueGestion.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iutlyon.banqueGestion.model.Message;

@Transactional
public interface MessageRepository extends JpaRepository<Message, Long> {

	List<Message> findByClientIdClientAndConseillerIdConseiller(Long idClient, Long idConseiller);
}
