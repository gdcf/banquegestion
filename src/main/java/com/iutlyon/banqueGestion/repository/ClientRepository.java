package com.iutlyon.banqueGestion.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iutlyon.banqueGestion.model.Client;
import com.iutlyon.banqueGestion.model.Conseiller;

@Transactional
public interface ClientRepository extends JpaRepository<Client, Long> {

	List<Client> findByConseillerIdConseiller(Long idConseiller);
	List<Client> findByCodeClient(String username);
}
