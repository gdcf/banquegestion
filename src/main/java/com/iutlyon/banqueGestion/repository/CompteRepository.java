package com.iutlyon.banqueGestion.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iutlyon.banqueGestion.model.Compte;

@Transactional
public interface CompteRepository extends JpaRepository<Compte, Long> {

	List<Compte> findByConseillerIdConseiller(Long idConseiller);
}
