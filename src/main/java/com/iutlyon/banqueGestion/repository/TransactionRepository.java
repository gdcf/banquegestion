package com.iutlyon.banqueGestion.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iutlyon.banqueGestion.model.Transaction;

@Transactional
public interface TransactionRepository extends JpaRepository<Transaction, Long>{

	List<Transaction> findByCompteIdCompte(Long idCompte);
}
