package com.iutlyon.banqueGestion;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.iutlyon.banqueGestion.model.Agence;
import com.iutlyon.banqueGestion.model.Client;
import com.iutlyon.banqueGestion.model.Compte;
import com.iutlyon.banqueGestion.model.CompteClient;
import com.iutlyon.banqueGestion.model.Conseiller;
import com.iutlyon.banqueGestion.model.Message;
import com.iutlyon.banqueGestion.repository.AgenceRepository;
import com.iutlyon.banqueGestion.repository.ClientRepository;
import com.iutlyon.banqueGestion.repository.CompteClientRepository;
import com.iutlyon.banqueGestion.repository.CompteRepository;
import com.iutlyon.banqueGestion.repository.ConseillerRepository;
import com.iutlyon.banqueGestion.repository.MessageRepository;
import com.iutlyon.banqueGestion.repository.TransactionRepository;

@SpringBootApplication
public class BanqueGestionApplication implements CommandLineRunner{

	@Autowired
	private AgenceRepository agenceRepository;
	
	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
	private CompteRepository compteRepository;
	
	@Autowired
	private CompteClientRepository compteClientRepository;
	
	@Autowired
	private ConseillerRepository conseillerRepository;
	
	@Autowired
	private MessageRepository messageRepository;
	
	@Autowired
	private TransactionRepository transactionRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(BanqueGestionApplication.class, args);	
	}
	
	@Override
	@SuppressWarnings("static-access")
	public void run(String... args) throws Exception {
	
		Agence agence = new Agence();
		agence.setAdresse("71000 Macon");
		agence.setHoraire("Ouvert tous les jours de 10h à 12h");
		agence.setNom("Banque Populaire Mâcon");
		agence.setNumeroTelephone("06.07.08.09.90");
		agenceRepository.save(agence);
		
		Conseiller conseiller = new Conseiller();
		conseiller.setAgence(agence);
		conseiller.setCodeConseiller("conseiller1");
		conseiller.setNom("HENRY");
		conseiller.setPrenom("Jacques");
		conseiller.setPassword("cons1");
		conseillerRepository.save(conseiller);
		
		Client client = new Client();
		client.setCodeClient("client1");
		client.setNom("DUPONT");
		client.setPrenom("Jean");
		client.setPassword("client1");
		client.setConseiller(conseiller);
		clientRepository.save(client);
		
		Message message = new Message();
		message.setClient(client);
		message.setConseiller(conseiller);
		message.setObjet("Lorem ipsum");
		message.setContenu("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
		messageRepository.save(message);
		
		
		Compte compte = new Compte();
		compte.setNom("compte 1");
		compte.setDecouvertAutorise((short)50);
		compte.setSolde((short)-60);
		compte.setConseiller(conseiller);
		compteRepository.save(compte);
		
		CompteClient compteClient = new CompteClient();
		compteClient.setClient(client);
		compteClient.setCompte(compte);
		compteClient.setIdClient(client.getIdClient());
		compteClient.setIdCompte(compte.getIdCompte());
		compteClientRepository.save(compteClient);
		}
}
