package com.iutlyon.banqueGestion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="t_message")
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
public class Message {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_message")
	private Long idMessage;
	private String objet;
	private String contenu;


	@ManyToOne
    @JoinColumn(name = "id_conseiller", referencedColumnName = "id_conseiller")
    private Conseiller conseiller;
	
	@ManyToOne
    @JoinColumn(name = "id_client", referencedColumnName = "id_client")
    private Client client;
}
