package com.iutlyon.banqueGestion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="t_agence")
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
public class Agence {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_agence")
	private Long idAgence;
	private String nom;
	private String numeroTelephone;
	private String adresse;
	private String horaire;
	
	

}
