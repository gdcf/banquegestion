package com.iutlyon.banqueGestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Embeddable
@Data
public class CompteClientPK implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Column(name="id_client")
	private Long idClient;
	
	
	@Column(name="id_compte")
	private Long idCompte;
}
