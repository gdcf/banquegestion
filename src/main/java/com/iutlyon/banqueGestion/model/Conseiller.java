package com.iutlyon.banqueGestion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="t_conseiller")
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
public class Conseiller {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_conseiller")
	private Long idConseiller;
	private String codeConseiller;
	private String nom;
	private String prenom;
	private String password;
	
	@ManyToOne
    @JoinColumn(name = "id_agence", referencedColumnName = "id_agence")
    private Agence agence;
}
