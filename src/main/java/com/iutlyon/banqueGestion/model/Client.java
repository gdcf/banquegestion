package com.iutlyon.banqueGestion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CollectionType;
import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="t_client")
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
public class Client {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_client")
	private Long idClient;
	private String codeClient;
	private String nom;
	private String prenom;
	private String password;
	
	@ManyToOne
	@JoinColumn(name = "id_conseiller", referencedColumnName = "id_conseiller")
	private Conseiller conseiller;
	
}