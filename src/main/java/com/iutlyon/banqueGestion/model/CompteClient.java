package com.iutlyon.banqueGestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="t_compte_x_client")
@Data
@AllArgsConstructor
@NoArgsConstructor
@IdClass(CompteClientPK.class)
public class CompteClient implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_compte")
	private Long idCompte;
	
	@Id
	@Column(name="id_client")
	private Long idClient;
	
	@ManyToOne
	@JoinColumn(name = "id_client", referencedColumnName = "id_client", insertable = false, updatable = false)
	Client client;
	
	@ManyToOne
	@JoinColumn(name = "id_compte", referencedColumnName = "id_compte", insertable = false, updatable = false)
	Compte compte;
	
}
