package com.iutlyon.banqueGestion.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="t_transaction")
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
public class Transaction  {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_transaction")
	private Long idTransaction;
	private short montant;
	private Date date;
	private String nom;
	
	@ManyToOne
    @JoinColumn(name = "id_compte", referencedColumnName = "id_compte")
    private Compte compte;
}