package com.iutlyon.banqueGestion.metier;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iutlyon.banqueGestion.model.Compte;
import com.iutlyon.banqueGestion.model.Transaction;
import com.iutlyon.banqueGestion.repository.ClientRepository;
import com.iutlyon.banqueGestion.repository.CompteClientRepository;
import com.iutlyon.banqueGestion.repository.CompteRepository;
import com.iutlyon.banqueGestion.repository.TransactionRepository;

@Service
public class CompteMetier {

	@Autowired
	CompteRepository compteRepository;
	
	@Autowired
	TransactionRepository transactionRepository;
	
	@Autowired
	ClientRepository clientRepository;
	
	@Autowired
	CompteClientRepository compteClientRepository;
	
	public void execVirement(String idCompte, String montant, String idCompteReceveur)
	{
		Compte compteDebite = compteRepository.getOne(Long.parseLong(idCompte));
		compteDebite.setSolde((short)(compteDebite.getSolde() - Short.parseShort(montant)));
		compteRepository.save(compteDebite);
		
		Compte compteReceveur = compteRepository.getOne(Long.parseLong(idCompteReceveur));
		compteReceveur.setSolde((short)(compteReceveur.getSolde() + Short.parseShort(montant)));
		compteRepository.save(compteReceveur);
		
		Transaction transaction = new Transaction();
		transaction.setCompte(compteDebite);
		transaction.setDate(new Date());
		transaction.setMontant(Short.parseShort(montant));
		transaction.setNom("VIREMENT VERS " + compteReceveur.getNom());
		transactionRepository.save(transaction);
		
		Transaction transaction2 = new Transaction();
		transaction2.setCompte(compteDebite);
		transaction2.setDate(new Date());
		transaction2.setMontant(Short.parseShort(montant));
		transaction2.setNom("RECEPTION VIREMENT DE " + compteDebite.getNom());
		transactionRepository.save(transaction2);
	}
}
